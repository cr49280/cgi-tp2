attribute vec4 vPosition;
//attribute vec4 vColor;
attribute vec4 vNormal;
//varying vec4 fColor;
uniform mat4 mProjection;
uniform mat4 mModelView;

void main(){
    gl_Position = mProjection * mModelView * vPosition;
    //fColor = vColor;
}